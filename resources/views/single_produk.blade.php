@extends('layouts.umum.app')
@section('meta')
<meta property='og:site_name' content='{{$details->nama}}'/>
<meta property='fb:app_id' content='136752056415945' />
<meta property='og:type' content='website' />
<meta property='og:url' content='{{url('produk/'.$details->id)}}' />
<meta property='og:title' content='{{$details->nama}}' />
<meta property='og:image' content='{{url('img/products/'.$details->gambarProduk->gambar)}}' />
<meta property='og:description' content='{{ucwords($details->tokoId->nama_toko)}}' />
@endsection
@section('css')
<!-- SPECIFIC CSS -->
<link href="{{asset('css/shop.css')}}" rel="stylesheet">
@endsection

@section('content')
<section class="parallax-window" data-parallax="scroll" data-image-src="{{asset('img/bg/single_tour_bg_1.jpg')}}" data-natural-width="1400" data-natural-height="470">
  <div class="parallax-content-1">
    <div class="animated fadeInDown">
      <h1>Detail Barang</h1>
    </div>
  </div>
</section>
<!-- End Section -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="#">Home</a>
        </li>
        <li><a href="#">Detail</a>
        </li>
        <li>{{$details->nama}}</li>
      </ul>
    </div>
  </div>
  <!-- End Position -->

  <div class="container margin_60">
    <div class="row">
      <div class="col-lg-9">

        <div class="product-details">

          <div class="basic-details">
            <div class="row">
              <div class="col-md-6">
                <div id="Img_carousel" class="slider-pro">
                  <div class="sp-slides">
                    @foreach($gambars as $key => $gambar)
                    <div class="sp-slide">
                      <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}" data-src="{{asset('img/products/'.$gambar->gambar)}}" data-small="{{asset('img/products/'.$gambar->gambar)}}" data-medium="{{asset('img/products/'.$gambar->gambar)}}" data-large="{{asset('img/products/'.$gambar->gambar)}}" data-retina="{{asset('img/products/'.$gambar->gambar)}}">
                    </div>
                    @endforeach

                  </div>
                  <div class="sp-thumbnails">
                    @foreach($gambars as $key => $gambarr)
                    <img alt="Image" class="sp-thumbnail" src="{{asset('img/products/'.$gambarr->gambar)}}">
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="info-column col-sm-6">
                <div class="details-header">
                  <h2>{{$details->nama}}</h2>
                  <div class="item-price">
                    <span class="offer">Rp {{number_format($details->harga*20/100+$details->harga)}}</span> Rp {{number_format($details->harga)}}
                  </div>
                  <div class="rating">
                    @if(count($comentars) > 0)
                    <?php $tot_nilai=0; ?>

                    @foreach($comentars as $key => $nilai)
                      <?php $tot_nilai+= $nilai->reting;?>
                    @endforeach
                    <!-- <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>  -->
                    <?php
                      $reting_rata2 = (int)$tot_nilai/count($comentars);
                     for ($i=0; $i < $reting_rata2-0.5; $i++) {
                      echo "<i class='icon-star voted'></i>";
                    }
                    $sisa = 5 - (int)$reting_rata2;
                     for ($e=0; $e < $sisa-0.5; $e++) {
                      echo "<i class='icon-star-empty'></i>";
                    } ?>
                    ({{count($comentars)}} Penilaian)
                    @endif
                    </div>
                </div>
                <div class="text">
                  <p>
                    {{$details->informasi}}
                  </p>
                </div>
                <div class="other-options">
                  <!-- <div class="numbers-row">
                    <input type="text" value="1" id="quantity_1" class="qty2 form-control" name="quantity_1">
                  </div> -->

                  <a href="{{route('masuk_keranjang',$details->id)}}" class="btn_1">Masuk Keranjang </a>
                </div>
                <!--Item Meta-->
                <ul class="item-meta">
                  <li>Kategori: <a href="#">{{$details->kategoriOneId->kategori}}</a>, <a href="#">{{$details->kategoriTwoId->kategori}}</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!--End Basic Details-->

          <div class="product-info-tabs">

            <div class="prod-tabs" id="product-tabs">
              <div class="tab-btns clearfix">
                <a href="#prod-description" class="tab-btn active-btn">Deskripsi</a>
                <a href="#prod-reviews" class="tab-btn">Penilaian</a>
              </div>

              <div class="tabs-container">
                <div class="tab active-tab" id="prod-description">
                  <h3>Deskripsi</h3>
                  <div class="content">
                    <p>
                      {{$details->informasi}}
                    </p>
                  </div>
                </div>
                <!--End Tab-->

                <div class="tab" id="prod-reviews">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
                  <h3>{{count($comentars)}} Penilaian</h3>
                  <div class="reviews-container">

                    @foreach($comentars as $key => $comentar)
                    <div class="review-box clearfix">
                      <figure class="rev-thumb">
                        @if($comentar->userId->foto !== null)
                        <img src="{{$comentar->userId->foto}}" />
                        @else

                        @endif
                      </figure>
                      <div class="rev-content">
                        <div class="rating">
                          <?php for ($i=0; $i < $comentar->reting; $i++) {
                            echo "<i class='icon-star voted'></i>";
                          } ?>
                          <?php
                          $sisa = 5 - (int)$comentar->reting;
                           for ($e=0; $e < $sisa; $e++) {
                            echo "<i class='icon-star-empty'></i>";
                          } ?>
                        </div>
                        <div class="rev-info">
                          {{$comentar->userId->name}} – {{date('d M Y', strtotime($comentar->created_at))}}
                        </div>
                        <div class="rev-text">
                          <p>
                            {!!$comentar->komentar!!}
                          </p>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  <!--End Review Container-->

                  <hr>

                  <div class="add-review">
                    <h3>Add a Review</h3>
                    <form method="post" action="{{route('detail_produk_saya',$details->id)}}">
                      @csrf
                      <input type="hidden" name="action" value="add_komen">
                      <div class="row">
                        <div class="form-group col-md-6">
                          <label>Rating </label>
                          <div class="rating">
                            <div class="payment-options">
              										<div class="radio-option">
              											<label for="reting-1"><input type="radio" name="reting" value="1" id="reting-1" checked><span class="icon-star voted"></span></label>
              										</div>
              										<div class="radio-option">
              											<label for="reting-2"><input type="radio" name="reting" value="2" id="reting-2"><span class="icon-star voted"><span class="icon-star voted"></label>
              										</div>
              										<div class="radio-option">
              											<label for="reting-3"><input type="radio" name="reting" value="3" id="reting-3"><span class="icon-star voted"><span class="icon-star voted"><span class="icon-star voted"></label>
              										</div>
              										<div class="radio-option">
              											<label for="reting-4"><input type="radio" name="reting" value="4" id="reting-4"><span class="icon-star voted"><span class="icon-star voted"><span class="icon-star voted"><span class="icon-star voted"></label>
              										</div>
              										<div class="radio-option">
              											<label for="reting-5"><input type="radio" name="reting" value="5" id="reting-5"><span class="icon-star voted"><span class="icon-star voted"><span class="icon-star voted"><span class="icon-star voted"><span class="icon-star voted"></label>
              										</div>
              							</div>
                          </div>
                        </div>
                        <div class="form-group col-md-12">
                          <label>Komentar</label>
                          <textarea id="komentar" name="komentar" class="form-control" style="height:150px;"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                           <button type="submit" class="btn_1">Kirim</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!--End tabs-container-->
            </div>
            <!--End prod-tabs-->
          </div>
          <!--End product-info-tabs-->

          <div class="related-products">
            <div class="normal-title">
              <h3>Related Products</h3>
            </div>
            <div class="row">
              <div class="shop-item col-lg-4 col-md-6 col-sm-6">
                <div class="inner-box">
                  <div class="image-box">
                    <figure class="image">
                      <a href="shop-single.html"><img src="{{asset('img/products/image-1.jpg')}}" alt=""></a>
                    </figure>
                    <div class="item-options clearfix">
                      <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                        <div class="tool-tip">Add to cart</div>
                      </a>
                      <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                        <div class="tool-tip">Add to Fav</div>
                      </a>
                      <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                        <div class="tool-tip">View</div>
                      </a>
                    </div>
                  </div>
                  <div class="product_description">
                    <div class="rating">
                      <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                    </div>
                    <h3><a href="shop-single.html">Travel Book</a></h3>
                    <div class="price">
                      <span class="offer">$20.00</span> $15.00
                    </div>
                  </div>
                </div>
              </div>
              <!--End Shop Item-->

              <div class="shop-item col-lg-4 col-md-6 col-sm-6">
                <div class="inner-box">
                  <div class="image-box">
                    <figure class="image">
                      <a href="shop-single.html"><img src="{{asset('img/products/image-2.jpg')}}" alt="">
                      </a>
                    </figure>
                    <div class="item-options clearfix">
                      <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                        <div class="tool-tip">Add to cart</div>
                      </a>
                      <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                        <div class="tool-tip">Add to Fav</div>
                      </a>
                      <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                        <div class="tool-tip">View</div>
                      </a>
                    </div>
                  </div>
                  <div class="product_description">
                    <div class="rating">
                      <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                    </div>
                    <h3><a href="shop-single.html">World guide</a></h3>
                    <div class="price">
                      <span class="offer">$10.00</span> $5.00
                    </div>
                  </div>
                </div>
              </div>
              <!--End Shop Item-->

              <div class="shop-item col-lg-4 col-md-6 col-sm-6">
                <div class="inner-box">
                  <div class="image-box">
                    <figure class="image">
                      <a href="shop-single.html"><img src="{{asset('img/products/image-3.jpg')}}" alt="">
                      </a>
                    </figure>
                    <div class="item-options clearfix">
                      <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                        <div class="tool-tip">Add to cart</div>
                      </a>
                      <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                        <div class="tool-tip">Add to Fav</div>
                      </a>
                      <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                        <div class="tool-tip">View</div>
                      </a>
                    </div>
                  </div>
                  <div class="product_description">
                    <div class="rating">
                      <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                    </div>
                    <h3><a href="shop-single.html">Best places to visit</a></h3>
                    <div class="price">
                      $22.00
                    </div>
                  </div>
                </div>
              </div>
              <!--End Shop Item-->

            </div>
          </div>
          <!--End Related products-->
        </div>
        <!--End Product-details-->
      </div>
      <!--End Col-->

      <div class="col-lg-3">
        <aside class="sidebar">
          <div class="widget">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" style="margin-left:0;"><i class="icon-search"></i></button>
              </span>
            </div>
          </div>
          <!-- End Search -->
          <hr>
          <div class="widget" id="cat_shop">
            <h4>Categories</h4>
            <ul>
              <li><a href="#">Places to visit</a>
              </li>
              <li><a href="#">Top tours</a>
              </li>
              <li><a href="#">Tips for travellers</a>
              </li>
              <li><a href="#">Events</a>
              </li>
            </ul>
          </div>
          <!-- End widget -->
          {{--<hr>
          <div class="widget">
            <h4>Filter</h4>
            <input type="text" id="range" name="range" value="">
          </div>--}}
          <!-- End widget -->
          <hr>
          <div class="widget related-products">
            <h4>Top Related </h4>
            <div class="post">
              <figure class="post-thumb">
                <a href="#"><img src="{{asset('img/products/image-4.jpg')}}" alt="">
                </a>
              </figure>
              <h5><a href="#">Grunge Fashion</a></h5>
              <div class="rating">
                <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
              </div>
              <div class="price">
                $ 15.00
              </div>
            </div>
            <div class="post">
              <figure class="post-thumb">
                <a href="#"><img src="{{asset('img/products/image-5.jpg')}}" alt="">
                </a>
              </figure>
              <h5><a href="#">Office Kit</a></h5>
              <div class="rating">
                <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
              </div>
              <div class="price">
                $ 15.00
              </div>
            </div>
            <div class="post">
              <figure class="post-thumb">
                <a href="#"><img src="{{asset('img/products/image-1.jpg')}}" alt="">
                </a>
              </figure>
              <h5><a href="#">Crime &amp; Punishment</a></h5>
              <div class="rating">
                <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
              </div>
              <div class="price">
                $ 15.00
              </div>
            </div>
          </div>
        </aside>
      </div>
      <!--Sidebar-->
    </div>
  </div>
  <!-- End Container -->
</main>
<!-- End main -->
@endsection
@section('javascript')
<!-- Date and time pickers -->
<script src="{{asset('js/jquery.sliderPro.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function ($) {
    $('#Img_carousel').sliderPro({
      width: 800,
      height: 900,
      fade: true,
      arrows: true,
      buttons: false,
      fullScreen: false,
      smallSize: 500,
      startSlide: 0,
      mediumSize: 1000,
      largeSize: 3000,
      thumbnailArrows: true,
      autoplay: false
    });
  });
</script>
<script>
  if ($('.prod-tabs .tab-btn').length) {
    $('.prod-tabs .tab-btn').on('click', function (e) {
      e.preventDefault();
      var target = $($(this).attr('href'));
      $('.prod-tabs .tab-btn').removeClass('active-btn');
      $(this).addClass('active-btn');
      $('.prod-tabs .tab').fadeOut(0);
      $('.prod-tabs .tab').removeClass('active-tab');
      $(target).fadeIn(500);
      $(target).addClass('active-tab');
    });

  }
</script>
<!-- Carousel -->
<script>
  $('.carousel-thumbs-2').owlCarousel({
  loop:false,
  margin:5,
  responsiveClass:true,
  nav:false,
  responsive:{
    0:{
      items:1
    },
    600:{
      items:3
    },
    1000:{
      items:4,
      nav:false
    }
  }
});
</script>

<!--Review modal validation -->
<script src="{{asset('assets/validate.js')}}"></script>

<script type="text/javascript">
  $('#access_login').magnificPopup({
  type: 'inline',
  fixedContentPos: true,
  fixedBgPos: true,
  overflowY: 'auto',
  closeBtnInside: true,
  preloader: false,
  midClick: true,
  removalDelay: 300,
  mainClass: 'my-mfp-zoom-in'
});
$('#keranjang').magnificPopup({
type: 'inline',
fixedContentPos: true,
fixedBgPos: true,
overflowY: 'auto',
closeBtnInside: true,
preloader: false,
midClick: true,
removalDelay: 300,
mainClass: 'my-mfp-zoom-in'
});
</script>

@endsection
