@extends('layouts.umum.app')
@section('icon_cari')
<li>
    <a href="javascript:void(0);" class="search-overlay-menu-btn"><i class="icon_search"></i></a>
</li>
@endsection
@section('form_search')
<div class="search-overlay-menu">
  <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
  <form role="search" id="searchform" method="get">
    <input value="" name="q" type="search" placeholder="Nama Toko... ?" />
    <button type="submit"><i class="icon_set_1_icon-78"></i>
    </button>
  </form>
</div>
@endsection
@section('content')
<?php $webs = App\Web::find(1); ?>
<section class="parallax-window" data-parallax="scroll" data-image-src="img/bg/single_hotel_bg_1.jpg" data-natural-width="1400" data-natural-height="470">
  <div class="parallax-content-1">
    <div class="animated fadeInDown">
      <h1>Pusat Belanja</h1>
      {{--<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>--}}
    </div>
  </div>
</section>
<!-- End section -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="#">Home</a>
        </li>
        <li><a href="#">Category</a>
        </li>
        <li>Page active</li>
      </ul>
    </div>
  </div>
  <!-- Position -->

  <div class="collapse" id="collapseMap">
    <div id="map" class="map"></div>
  </div>
  <!-- End Map -->

  <div class="container margin_60">
    <div class="row">
      <aside class="col-lg-3">

        <div id="filters_col">
          <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Filters</a>
          <div class="collapse show" id="collapseFilters">
            <!-- <div class="filter_type">
              <h6>Price</h6>
              <input type="text" id="range" name="range" value="">
            </div> -->
            <div class="filter_type">
              <h6>Rating</h6>
              <ul>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
              </ul>
            </div>
            <div class="filter_type">
              <h6>Facility</h6>
              <ul>
                <li>
                  <label>
                    <input type="checkbox">Pet allowed
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox">Groups allowed
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox">Tour guides
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox">Access for disabled
                  </label>
                </li>
              </ul>
            </div>
          </div>
          <!--End collapse -->
        </div>
        <!--End filters col-->
        <div class="box_style_2">
          <i class="icon_set_1_icon-57"></i>
          <h4>Need <span>Help?</span></h4>
          <a href="tel://004542344599" class="phone">{{$webs->phone}}</a>
          <small>Monday to Friday 9.00am - 7.30pm</small>
        </div>
      </aside>
      <!--End aside -->

      <div class="col-lg-9">

        <div id="tools">
          <div class="row">
            <!-- <div class="col-md-3 col-sm-4 col-6">
              <div class="styled-select-filters">
                <select name="sort_price" id="sort_price">
                  <option value="" selected>Sort by price</option>
                  <option value="lower">Lowest price</option>
                  <option value="higher">Highest price</option>
                </select>
              </div>
            </div> -->
            <div class="col-md-3 col-sm-4 col-6">
              <div class="styled-select-filters">
                <select name="sort_rating" id="sort_rating">
                  <option value="" selected>Sortir Berdasarkan ?</option>
                  <option value="name">Nama Toko</option>
                  <option value="reting">Reting Toko</option>
                </select>
              </div>
            </div>
            <!-- <div class="col-md-6 col-sm-4 d-none d-sm-block text-right">
              <a href="#" class="bt_filters"><i class="icon-th"></i></a> <a href="all_tours_list.html" class="bt_filters"><i class=" icon-list"></i></a>
            </div> -->
          </div>
        </div>

        <div class="row">
          @foreach($tokos as $key => $toko)
          <div class="col-md-6 wow zoomIn" data-wow-delay="0.1s">
            <div class="transfer_container">
              <div class="ribbon_3 popular"><span>Popular</span>
              </div>
              <div class="img_container">
                <a href="{{route('toko',$toko->id)}}">
                  <img src="{{asset('img/toko/'.$toko->gambar)}}" width="800" height="533" class="img-fluid" alt="Image">
                  <div class="short_info">
                    <!-- From/Per person--><span class="price"><sup>{{ucwords($toko->nama_toko)}}</sup></span>
                  </div>
                </a>
              </div>
              <div class="transfer_title">
                <h3><strong>Kec.</strong> {{ucwords($toko->kecamatan)}}</h3>
                <div class="rating">
                  <?php for ($i=0; $i < $toko->reting; $i++) {
                    echo "<i class='icon-smile voted'></i>";
                  } ?>
                  <?php
                  $sisa = 5 - (int)$toko->reting;
                   for ($e=0; $e < $sisa; $e++) {
                    echo "<i class='icon-smile'></i>";
                  } ?>

                  <!-- <i class="icon-smile voted"></i>
                  <i class="icon-smile voted"></i>
                  <i class="icon-smile voted"></i>
                  <i class="icon-smile"></i> -->
                  <!-- <small>(75)</small> -->
                </div>
                <!-- end rating -->
                <div class="wishlist">
                  <a class="tooltip_flip tooltip-effect-1" href="#">+<span class="tooltip-content-flip">{{--<span class="tooltip-back">Add to wishlist</span>--}}</span></a>
                </div>
                <!-- End wish list-->
              </div>
            </div>
            <!-- End box tour -->
          </div>
          @endforeach
          <!-- End col-md-6 -->

          {{--<div class="col-md-6 wow zoomIn" data-wow-delay="0.2s">
            <div class="transfer_container">
              <div class="ribbon_3 popular"><span>Popular</span>
              </div>
              <div class="img_container">
                <a href="{{route('toko',1)}}">
                  <img src="img/transfer_1.jpg" width="800" height="533" class="img-fluid" alt="Image">
                  <div class="short_info">
                    <!-- From/Per person--><span class="price"><sup>NAMA TOKO</sup></span>
                  </div>
                </a>
              </div>
              <div class="transfer_title">
                <h3><strong>Kec.</strong> Sape</h3>
                <div class="rating">
                  <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <!-- end rating -->
                <div class="wishlist">
                  <a class="tooltip_flip tooltip-effect-1" href="#">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                </div>
                <!-- End wish list-->
              </div>
            </div>
            <!-- End box tour -->
          </div>--}}
          <!-- End col-md-6 -->
        </div>
        <!-- End row -->

        {{--<div class="row">
          <div class="col-md-6 wow zoomIn" data-wow-delay="0.2s">
            <div class="transfer_container">
              <div class="ribbon_3 popular"><span>Popular</span>
              </div>
              <div class="img_container">
                <a href="{{route('toko',1)}}">
                  <img src="img/transfer_3.jpg" width="800" height="533" class="img-fluid" alt="Image">
                  <div class="short_info">
                    <!-- From/Per person--><span class="price"><sup>NAMA TOKO</sup></span>
                  </div>
                </a>
              </div>
              <div class="transfer_title">
                <h3><strong>Kec.</strong> Sape</h3>
                <div class="rating">
                  <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <!-- end rating -->
                <div class="wishlist">
                  <a class="tooltip_flip tooltip-effect-1" href="#">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                </div>
                <!-- End wish list-->
              </div>
            </div>
            <!-- End box tour -->
          </div>
          <!-- End col-md-6 -->

          <div class="col-md-6 wow zoomIn" data-wow-delay="0.4s">
            <div class="transfer_container">
              <div class="ribbon_3"><span>Top rated</span>
              </div>
              <div class="img_container">
                <a href="{{route('toko',1)}}">
                  <img src="img/transfer_4.jpg" width="800" height="533" class="img-fluid" alt="Image">
                  <div class="short_info">
                    <!-- From/Per person--><span class="price"><sup>NAMA TOKO</sup></span>
                  </div>
                </a>
              </div>
              <div class="transfer_title">
                <h3><strong>Kec.</strong> Sape</h3>
                <div class="rating">
                  <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <!-- end rating -->
                <div class="wishlist">
                  <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                </div>
                <!-- End wish list-->
              </div>
            </div>
            <!-- End box tour -->
          </div>
          <!-- End col-md-6 -->
        </div>--}}
        <!-- End row -->

        <hr>

        <nav aria-label="Page navigation">
          <ul class="pagination justify-content-center">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item active"><span class="page-link">1<span class="sr-only">(current)</span></span>
            </li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- end pagination-->

      </div>
      <!-- End col lg 9 -->
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</main>
<!-- End main -->
@endsection
@section('javascript')
<!-- Check box and radio style iCheck -->
<script>
  $('input').iCheck({
     checkboxClass: 'icheckbox_square-grey',
     radioClass: 'iradio_square-grey'
   });
</script>
@endsection
