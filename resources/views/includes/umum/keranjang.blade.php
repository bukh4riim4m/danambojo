@guest
@else
<?php $kerangjangs = App\Keranjang::where('aktif','yes')->where('user_id',Auth::user()->id)->get(); ?>
@endguest
<ul id="top_tools">
    @yield('icon_cari')
    <li>
        <div class="dropdown dropdown-cart">
            <a href="#" data-toggle="dropdown" @guest  @else class="cart_bt" @endguest><i class="icon_bag_alt"></i><strong>@guest  @else {{count($kerangjangs)}} @endguest</strong></a>
            @guest
            @else
            <ul class="dropdown-menu" id="cart_items">
              <?php $total=0; ?>
              @foreach($kerangjangs as $kerangjang)
              <?php $total+=$kerangjang->produkId->harga*$kerangjang->jumlah;?>
                <li>
                    <div class="image"><img src="{{asset('img/products/'.$kerangjang->produkId->gambarProduk->gambar)}}" alt="image"></div>
                    <strong><a href="#">{{$kerangjang->produkId->nama}}</a>{{$kerangjang->jumlah}}x Rp {{number_format($kerangjang->produkId->harga)}} </strong>
                    <a href="{{route('hapus_keranjang',$kerangjang->id)}}" class="action"><i class="icon-trash"></i></a>
                </li>
                @endforeach
                <li>
                    <div>Total: <span>Rp {{number_format($total)}}</span></div>
                    <a href="{{route('keranjang')}}" class="button_drop">Keranjang</a>
                    <a href="payment.html" class="button_drop outline">Proses</a>
                </li>
            </ul>
            @endguest
        </div><!-- End dropdown-cart-->
    </li>
</ul>
