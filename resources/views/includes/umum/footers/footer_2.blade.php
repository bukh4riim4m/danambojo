<?php $webs = App\Web::find(1); ?>
<footer id="pattern_2" class="revealed">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-sm-12">
                  <p><img src="{{asset('img/dana_mbojo/logo.png')}}" width="160" height="34" alt="City tours" data-retina="true" id="logo"></p>
        <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p>
              </div>
              <div class="col-md-3 col-sm-4">
                  <h3>About</h3>
                  <ul>
                      <li><a href="{{route('tentang_kami')}}">Tentang Kami</a></li>
                      <li><a href="{{route('pertanyaan')}}">Pertanyan</a></li>
                      <li><a href="{{route('login')}}">Login</a></li>
                      <li><a href="{{route('register')}}">Daftar</a></li>
                       <li><a href="{{route('hubungi_kami')}}">Hubungi Kami</a></li>
                  </ul>
              </div>
              <div class="col-md-3 col-sm-4">
                  <h3>Discover</h3>
                  <ul>
                      <li><a href="#">Community blog</a></li>
                      <li><a href="#">Tour guide</a></li>
                      <li><a href="#">Wishlist</a></li>
                       <li><a href="#">Gallery</a></li>
                  </ul>
              </div>
              <div class="col-md-2 col-sm-4">
                  <h3>Settings</h3>
                  <div class="styled-select">
                      <select name="lang" id="lang">
                          <option value="English" selected>English</option>
                          <option value="French">French</option>
                          <option value="Spanish">Spanish</option>
                          <option value="Russian">Russian</option>
                      </select>
                  </div>
                  <div class="styled-select">
                      <select name="currency" id="currency">
                          <option value="USD" selected>USD</option>
                          <option value="EUR">EUR</option>
                          <option value="GBP">GBP</option>
                          <option value="RUB">RUB</option>
                      </select>
                  </div>
              </div>
          </div><!-- End row -->
          <div class="row">
              <div class="col-md-12">
                  <div id="social_footer">
                      <ul>
                          <li><a href="#"><i class="icon-facebook"></i></a></li>
                          <li><a href="#"><i class="icon-twitter"></i></a></li>
                          <li><a href="#"><i class="icon-google"></i></a></li>
                          <li><a href="#"><i class="icon-instagram"></i></a></li>
                          <li><a href="#"><i class="icon-pinterest"></i></a></li>
                          <li><a href="#"><i class="icon-vimeo"></i></a></li>
                          <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                      </ul>
                      <p>© {{$webs->judul}} 2019</p>
                  </div>
              </div>
          </div><!-- End row -->
      </div><!-- End container -->
  </footer><!-- End footer -->
