<div class="search-overlay-menu">
  <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
  <form role="search" id="searchform" method="get">
    <input value="" name="q" type="search" placeholder="Search..." />
    <button type="submit"><i class="icon_set_1_icon-78"></i>
    </button>
  </form>
</div>
