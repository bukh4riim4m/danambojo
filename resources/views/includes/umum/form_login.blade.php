<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
  <div class="small-dialog-header">
    <h3>Sign In</h3>
  </div>
  <form method="POST" action="{{ route('login') }}">
      @csrf
    <div class="sign-in-wrapper">
      <a href="{{ url('/auth/facebook') }}" class="social_bt facebook">Login with Facebook</a>
      {{--<a href="#0" class="social_bt google">Login with Google</a>--}}
      <div class="divider"><span>Or</span></div>
      <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <i class="icon_mail_alt"></i>
      </div>
      <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control @error('password') is-invalid @enderror" value="" name="password" id="password" required autocomplete="current-password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <i class="icon_lock_alt"></i>
      </div>
      <div class="clearfix add_bottom_15">
        <div class="checkboxes float-left">
          <input id="remember-me" type="checkbox" name="check">
          <label for="remember-me">Remember Me</label>
        </div>
        <div class="float-right"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
      </div>
      <div class="text-center"><input type="submit" value="Log In" class="btn_login"></div>
      <div class="text-center">
        Don’t have an account? <a href="{{route('register')}}">Sign up</a>
      </div>
      </form>
      <div id="forgot_pw">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="form-group">
          <label>Harap konfirmasi email masuk di bawah ini</label>
          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email">
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
          <i class="icon_mail_alt"></i>
        </div>
        <p>Anda akan menerima email yang berisi tautan yang memungkinkan Anda mengatur ulang kata sandi Anda ke kata sandi pilihan baru.</p>
        <div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
        </form>
      </div>
    </div>

  <!--form -->
</div>
