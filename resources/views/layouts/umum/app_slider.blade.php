<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Dana Mbojo - Websitenya orang bima loh...">
    <meta name="author" content="Imam Bukhari">
    @yield('meta')
    <title>{{config('app.name')}}</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('img/logo_antartravel_green.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/apple-touch-icon-57x57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('img/apple-touch-icon-72x72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('img/apple-touch-icon-114x114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('img/apple-touch-icon-144x144-precomposed.png')}}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,700" rel="stylesheet">

    <!-- COMMON CSS -->
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
	<link href="{{asset('css/vendors.css')}}" rel="stylesheet">

	<!-- CUSTOM CSS -->
	<link href="{{asset('css/custom.css')}}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('rev-slider-files/fonts/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('rev-slider-files/css/settings.css')}}">

    <!-- REVOLUTION LAYERS STYLES -->
    <style>
  		.tp-caption.NotGeneric-Title,
  		.NotGeneric-Title {
  			color: rgba(255, 255, 255, 1.00);
  			font-size: 70px;
  			line-height: 70px;
  			font-weight: 800;
  			font-style: normal;
  			text-decoration: none;
  			background-color: transparent;
  			border-color: transparent;
  			border-style: none;
  			border-width: 0px;
  			border-radius: 0 0 0 0px
  		}

  		.tp-caption.NotGeneric-SubTitle,
  		.NotGeneric-SubTitle {
  			color: rgba(255, 255, 255, 1.00);
  			font-size: 13px;
  			line-height: 20px;
  			font-weight: 500;
  			font-style: normal;
  			text-decoration: none;
  			background-color: transparent;
  			border-color: transparent;
  			border-style: none;
  			border-width: 0px;
  			border-radius: 0 0 0 0px;
  			letter-spacing: 4px
  		}

  		.tp-caption.NotGeneric-Icon,
  		.NotGeneric-Icon {
  			color: rgba(255, 255, 255, 1.00);
  			font-size: 30px;
  			line-height: 30px;
  			font-weight: 400;
  			font-style: normal;
  			text-decoration: none;
  			background-color: rgba(0, 0, 0, 0);
  			border-color: rgba(255, 255, 255, 0);
  			border-style: solid;
  			border-width: 0px;
  			border-radius: 0px 0px 0px 0px;
  			letter-spacing: 3px
  		}

  		.tp-caption.NotGeneric-Button,
  		.NotGeneric-Button {
  			color: rgba(255, 255, 255, 1.00);
  			font-size: 14px;
  			line-height: 14px;
  			font-weight: 500;
  			font-style: normal;
  			text-decoration: none;
  			background-color: rgba(0, 0, 0, 0);
  			border-color: rgba(255, 255, 255, 0.50);
  			border-style: solid;
  			border-width: 1px;
  			border-radius: 0px 0px 0px 0px;
  			letter-spacing: 3px
  		}

  		.tp-caption.NotGeneric-Button:hover,
  		.NotGeneric-Button:hover {
  			color: rgba(255, 255, 255, 1.00);
  			text-decoration: none;
  			background-color: transparent;
  			border-color: rgba(255, 255, 255, 1.00);
  			border-style: solid;
  			border-width: 1px;
  			border-radius: 0px 0px 0px 0px;
  			cursor: pointer
  		}
  	</style>
</head>
<body>

    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->
  @include('includes.umum.header')
    <!-- Header================================================== -->
  @yield('content')
	<!-- End main -->
  @include('includes.umum.footers.footer_2')
	<div id="toTop"></div><!-- Back to top button -->
	<!-- Search Menu -->
  @yield('form_search')
	<!-- End Search Menu -->
	<!-- Sign In Popup -->
  @include('includes.umum.form_login')

	<!-- /Sign In Popup -->

    <!-- Common scripts -->
    <script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/common_scripts_min.js')}}"></script>
    <script src="{{asset('js/functions.js')}}"></script>

    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script src="{{asset('rev-slider-files/js/jquery.themepunch.tools.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/jquery.themepunch.revolution.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.actions.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.carousel.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.migration.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.navigation.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.parallax.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
	<script src="{{asset('rev-slider-files/js/extensions/revolution.extension.video.min.js')}}"></script>
	<script>
		var tpj = jQuery;
		var revapi54;
		tpj(document).ready(function () {
			if (tpj("#rev_slider_54_1").revolution == undefined) {
				revslider_showDoubleJqueryError("#rev_slider_54_1");
			} else {
				revapi54 = tpj("#rev_slider_54_1").show().revolution({
					sliderType: "standard",
					jsFileLocation: "rev-slider-files/js/",
					sliderLayout: "fullwidth",
					dottedOverlay: "none",
					delay: 9000,
					navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
                             mouseScrollReverse:"default",
							onHoverStop:"off",
							touch:{
								touchenabled:"on",
								touchOnDesktop:"off",
								swipe_threshold: 75,
								swipe_min_touches: 50,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							}
							,
							arrows: {
								style:"uranus",
								enable:true,
								hide_onmobile:true,
								hide_under:778,
								hide_onleave:true,
								hide_delay:200,
								hide_delay_mobile:1200,
								tmp:'',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:20,
                                    v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:20,
                                    v_offset:0
								}
							}
						},
					responsiveLevels: [1240, 1024, 778, 480],
					visibilityLevels: [1240, 1024, 778, 480],
					gridwidth: [1240, 1024, 778, 480],
					gridheight: [700, 550, 860, 480],
					lazyType: "none",
					parallax: {
						type: "mouse",
						origo: "slidercenter",
						speed: 2000,
						levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
						disable_onmobile: "off"
					},
					shadow: 0,
					spinner: "off",
					stopLoop: "off",
					stopAfterLoops: 0,
					stopAtSlide: 0,
					shuffle: "off",
					autoHeight: "off",
					disableProgressBar: "on",
					hideThumbsOnMobile: "off",
					hideSliderAtLimit: 0,
					hideCaptionAtLimit: 0,
					hideAllCaptionAtLilmit: 0,
					debugMode: false,
					fallbacks: {
						simplifyAll: "off",
						nextSlideOnWindowFocus: "off",
						disableFocusListener: false,
					}
				});
			}
		}); /*ready*/
	</script>

	<!-- NOTIFY BUBBLES  -->
@yield('javascript')

</body>

</html>
