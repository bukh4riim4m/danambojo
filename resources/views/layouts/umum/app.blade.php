<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Dana Mbojo - Websitenya orang bima loh...">
    <meta name="author" content="Imam Bukhari">
    @yield('meta')
    <title>{{config('app.name')}}</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('img/logo_antartravel_green.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/apple-touch-icon-57x57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('img/apple-touch-icon-72x72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('img/apple-touch-icon-114x114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('img/apple-touch-icon-144x144-precomposed.png')}}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,700" rel="stylesheet">

    <!-- COMMON CSS -->
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
	<link href="{{asset('css/vendors.css')}}" rel="stylesheet">

	<!-- CUSTOM CSS -->
	<link href="{{asset('css/custom.css')}}" rel="stylesheet">
  @yield('css')

    <!-- REVOLUTION SLIDER CSS -->
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('rev-slider-files/fonts/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('rev-slider-files/css/settings.css')}}"> -->

    <!-- REVOLUTION LAYERS STYLES -->
	<!-- <style>
		.tp-caption.Travel-BigCaption,
		.Travel-BigCaption {
			color: rgba(255, 255, 255, 1.00);
			font-size: 50px;
			line-height: 50px;
			font-weight: 700;
			font-style: normal;
			text-transform: uppercase;
			text-decoration: none;
			background-color: transparent;
			border-color: transparent;
			border-style: none;
			border-width: 0px;
			border-radius: 0 0 0 0px
		}

		.tp-caption.Travel-SmallCaption,
		.Travel-SmallCaption {
			color: rgba(255, 255, 255, 1.00);
			font-size: 25px;
			line-height: 30px;
			font-weight: 400;
			font-family: "Lato";
			font-style: normal;
			text-decoration: none;
			background-color: transparent;
			border-color: transparent;
			border-style: none;
			border-width: 0px;
			border-radius: 0 0 0 0px
		}
	</style>
	<style type="text/css">
		.hermes.tp-bullets {}

		.hermes .tp-bullet {
			overflow: hidden;
			border-radius: 50%;
			width: 16px;
			height: 16px;
			background-color: rgba(0, 0, 0, 0);
			box-shadow: inset 0 0 0 2px rgb(255, 255, 255);
			-webkit-transition: background 0.3s ease;
			transition: background 0.3s ease;
			position: absolute
		}

		.hermes .tp-bullet:hover {
			background-color: rgba(0, 0, 0, 0.21)
		}

		.hermes .tp-bullet:after {
			content: ' ';
			position: absolute;
			bottom: 0;
			height: 0;
			left: 0;
			width: 100%;
			background-color: rgb(255, 255, 255);
			box-shadow: 0 0 1px rgb(255, 255, 255);
			-webkit-transition: height 0.3s ease;
			transition: height 0.3s ease
		}

		.hermes .tp-bullet.selected:after {
			height: 100%
		}
	</style> -->
</head>
<body>

    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->
  @include('includes.umum.header')
    <!-- Header================================================== -->
  @yield('content')
	<!-- End main -->
  @include('includes.umum.footers.footer_2')
	<div id="toTop"></div><!-- Back to top button -->
	<!-- Search Menu -->
  @yield('form_search')
	<!-- End Search Menu -->
	<!-- Sign In Popup -->
  @include('includes.umum.form_login')

	<!-- /Sign In Popup -->

    <!-- Common scripts -->
    <script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/common_scripts_min.js')}}"></script>
    <script src="{{asset('js/functions.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="{{ asset('js/share.js') }}"></script>

    @yield('javascript')
</body>

</html>
