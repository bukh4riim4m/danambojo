@extends('layouts.umum.app')

@section('icon_cari')
<li>
    <a href="javascript:void(0);" class="search-overlay-menu-btn"><i class="icon_search"></i></a>
</li>
@endsection
@section('form_search')
<div class="search-overlay-menu">
  <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
  <form role="search" id="searchform" method="get">
    <input value="" name="q" type="search" placeholder="Nama Produk... ?" />
    <button type="submit"><i class="icon_set_1_icon-78"></i>
    </button>
  </form>
</div>
@endsection
@section('css')
<link href="{{asset('css/shop.css')}}" rel="stylesheet">
@endsection

@section('content')
<section class="parallax-window" data-parallax="scroll" data-image-src="{{asset('img/toko/'.$tokos->gambar)}}" data-natural-width="1400" data-natural-height="470">
  <div class="parallax-content-1">
    <div class="animated fadeInDown">
      <h1>{{$tokos->nama_toko}}</h1>
    </div>
  </div>
</section>
<!-- End Section -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="#">Home</a>
        </li>
        <li><a href="#">Toko Saya</a>
        </li>
        <li>{{$tokos->nama_toko}}</li>
      </ul>
    </div>
  </div>
  <!-- End Position -->

  <div class="container margin_60">
    <div class="row">
      <div class="col-lg-9">
        <div class="shop-section">

          <div class="items-sorting">
            <div class="row">
              <div class="col-6">
                <div class="results_shop">
                  Showing 1–9 of 15 results
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <select name="sort-by">
                    <option>Sorting by</option>
                    <option>Order</option>
                    <option>Price</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <!--End Sort By-->

          <div class="row">
            @foreach($produks as $key => $produk)
            <div class="shop-item col-lg-4 col-md-6 col-sm-6">
              <div class="inner-box">
                <div class="image-box">
                  <figure class="image">
                    <a href="{{route('detail_produk_saya',$produk->id)}}"><img src="{{asset('img/products/'.$produk->gambarProduk->gambar)}}" alt="">
                    </a>
                  </figure>
                  <div class="item-options clearfix">
                    <a href="shopping-cart.html" class="btn_shop"><span class="icon-edit"></span>
                                          <div class="tool-tip">
                                              Edit
                                          </div>
                                          </a>
                    <!-- <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                                          <div class="tool-tip">
                                              Add to Fav
                                          </div>
                                          </a> -->
                    <a href="{{route('detail_produk_saya',$produk->id)}}" class="btn_shop"><span class="icon-eye"></span>
                                          <div class="tool-tip">
                                              Detail
                                          </div>
                                          </a>
                  </div>
                </div>
                <div class="product_description">
                  <div class="rating">
                    <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                  </div>
                  <h3><a href="shop-single.html">{{$produk->nama}}</a></h3>
                  <div class="price">
                     Rp {{number_format($produk->harga)}}
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            <!--End Shop Item-->

            {{--<div class="shop-item col-lg-4 col-md-6 col-sm-6">
              <div class="inner-box">
                <div class="image-box">
                  <figure class="image">
                    <a href="shop-single.html"><img src="{{asset('img/products/image-2.jpg')}}" alt="">
                    </a>
                  </figure>
                  <div class="item-options clearfix">
                    <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                                          <div class="tool-tip">
                                              Add to cart
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                                          <div class="tool-tip">
                                              Add to Fav
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                                          <div class="tool-tip">
                                              View
                                          </div>
                                          </a>
                  </div>
                </div>
                <div class="product_description">
                  <div class="rating">
                    <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                  </div>
                  <h3><a href="shop-single.html">World guide</a></h3>
                  <div class="price">
                    <span class="offer">$10.00</span> $5.00
                  </div>
                </div>
              </div>
            </div>
            <!--End Shop Item-->

            <div class="shop-item col-lg-4 col-md-6 col-sm-6">
              <div class="inner-box">
                <div class="image-box">
                  <figure class="image">
                    <a href="shop-single.html"><img src="{{asset('img/products/image-3.jpg')}}" alt="">
                    </a>
                  </figure>
                  <div class="item-options clearfix">
                    <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                                          <div class="tool-tip">
                                              Add to cart
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                                          <div class="tool-tip">
                                              Add to Fav
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                                          <div class="tool-tip">
                                              View
                                          </div>
                                          </a>
                  </div>
                </div>
                <div class="product_description">
                  <div class="rating">
                    <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                  </div>
                  <h3><a href="shop-single.html">Best places to visit</a></h3>
                  <div class="price">
                    $22.00
                  </div>
                </div>
              </div>
            </div>
            <!--End Shop Item-->

            <div class="shop-item col-lg-4 col-md-6 col-sm-6">
              <div class="inner-box">
                <!--Image Box-->
                <div class="image-box">
                  <figure class="image">
                    <a href="shop-single.html"><img src="{{asset('img/products/image-4.jpg')}}" alt="">
                    </a>
                  </figure>
                  <div class="item-options clearfix">
                    <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                                          <div class="tool-tip">
                                              Add to cart
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                                          <div class="tool-tip">
                                              Add to Fav
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                                          <div class="tool-tip">
                                              View
                                          </div>
                                          </a>
                  </div>
                </div>
                <div class="product_description">
                  <div class="rating">
                    <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                  </div>
                  <h3><a href="shop-single.html">World Streets</a></h3>
                  <div class="price">
                    $22.00
                  </div>
                </div>
              </div>
            </div>
            <!--End Shop Item-->

            <div class="shop-item col-lg-4 col-md-6 col-sm-6">
              <div class="inner-box">
                <div class="image-box">
                  <figure class="image">
                    <a href="shop-single.html"><img src="{{asset('img/products/image-5.jpg')}}" alt="">
                    </a>
                  </figure>
                  <div class="item-options clearfix">
                    <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                                          <div class="tool-tip">
                                              Add to cart
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                                          <div class="tool-tip">
                                              Add to Fav
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                                          <div class="tool-tip">
                                              View
                                          </div>
                                          </a>
                  </div>
                </div>
                <div class="product_description">
                  <div class="rating">
                    <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                  </div>
                  <h3><a href="shop-single.html">Travel guide V2</a></h3>
                  <div class="price">
                    $15.00
                  </div>
                </div>
              </div>
            </div>
            <!--End Shop Item-->

            <div class="shop-item col-lg-4 col-md-6 col-sm-6">
              <div class="inner-box">
                <!--Image Box-->
                <div class="image-box">
                  <figure class="image">
                    <a href="shop-single.html"><img src="{{asset('img/products/image-6.jpg')}}" alt="">
                    </a>
                  </figure>
                  <div class="item-options clearfix">
                    <a href="shopping-cart.html" class="btn_shop"><span class="icon-basket"></span>
                                          <div class="tool-tip">
                                              Add to cart
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-heart-8"></span>
                                          <div class="tool-tip">
                                              Add to Fav
                                          </div>
                                          </a>
                    <a href="shop-single.html" class="btn_shop"><span class="icon-eye"></span>
                                          <div class="tool-tip">
                                              View
                                          </div>
                                          </a>
                  </div>
                </div>
                <div class="product_description">
                  <div class="rating">
                    <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                  </div>
                  <h3><a href="shop-single.html">Adventures</a></h3>
                  <div class="price">
                    <span class="offer">$20.00</span> $15.00
                  </div>
                </div>
              </div>
            </div>--}}
          </div>
          <!--End Shop Item-->

          <hr>

          <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center">
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active"><span class="page-link">1<span class="sr-only">(current)</span></span>
              </li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
          <!-- end pagination-->

        </div>
        <!-- End row -->
      </div>
      <!-- End col -->

      <!--Sidebar-->
      <div class="col-lg-3">
        <aside class="sidebar">
          <div class="widget">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                      <button class="btn btn-default" type="button" style="margin-left:0;"><i class="icon-search"></i></button>
                      </span>
            </div>
          </div>
          <!-- End Search -->
          <hr>
          <div class="widget" id="cat_shop">
            <h4>Categories</h4>
            <ul>
              <li><a href="#">Places to visit</a>
              </li>
              <li><a href="#">Top tours</a>
              </li>
              <li><a href="#">Tips for travellers</a>
              </li>
              <li><a href="#">Events</a>
              </li>
            </ul>
          </div>
          <!-- End widget -->
          <hr>
          <div class="widget">
            <h4>Share ke Sosmed</h4>
            <?php $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>
            	<a href="https://www.facebook.com/sharer/sharer.php?u={{url('toko/'.$tokos->id)}}" class="social_bt facebook social-button" id="">Share Toko Ke Facebook</a>
              <!-- <a href="{{route('share')}}" class="social-button " id=""><span class="fa fa-facebook-official">FBR</span></a> -->
          </div>
          <!-- End widget -->
          <hr>
          <div class="widget related-products">
            <h4>Top Related </h4>
            <div class="post">
              <figure class="post-thumb">
                <a href="#"><img src="{{asset('img/products/thumb-1.jpg')}}" alt="">
                </a>
              </figure>
              <h5><a href="#">Grunge Fashion</a></h5>
              <div class="rating">
                <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
              </div>
              <div class="price">
                $ 15.00
              </div>
            </div>
            <div class="post">
              <figure class="post-thumb">
                <a href="#"><img src="{{asset('img/products/thumb-2.jpg')}}" alt="">
                </a>
              </figure>
              <h5><a href="#">Office Kit</a></h5>
              <div class="rating">
                <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
              </div>
              <div class="price">
                $ 15.00
              </div>
            </div>
            <div class="post">
              <figure class="post-thumb">
                <a href="#"><img src="{{asset('img/products/thumb-3.jpg')}}" alt="">
                </a>
              </figure>
              <h5><a href="#">Crime &amp; Punishment</a></h5>
              <div class="rating">
                <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
              </div>
              <div class="price">
                $ 15.00
              </div>
            </div>
          </div>
        </aside>
      </div>
      <!--Sidebar-->
    </div>
  </div>
  <!-- End Container -->
</main>
<!-- End main -->
@endsection
