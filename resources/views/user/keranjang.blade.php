@extends('layouts.umum.app')
@section('content')
<section id="hero_2">
  <div class="intro_title">
    <h1>PESANAN</h1>
    <div class="bs-wizard row">

      <div class="col-4 bs-wizard-step active">
        <div class="text-center bs-wizard-stepnum">Keranjang Saya</div>
        <div class="progress">
          <div class="progress-bar"></div>
        </div>
        <a href="cart.html" class="bs-wizard-dot"></a>
      </div>

      <div class="col-4 bs-wizard-step disabled">
        <div class="text-center bs-wizard-stepnum">Detail Pesanan</div>
        <div class="progress">
          <div class="progress-bar"></div>
        </div>
        <a href="payment.html" class="bs-wizard-dot"></a>
      </div>

      <div class="col-4 bs-wizard-step disabled">
        <div class="text-center bs-wizard-stepnum">Selesai</div>
        <div class="progress">
          <div class="progress-bar"></div>
        </div>
        <a href="confirmation.html" class="bs-wizard-dot"></a>
      </div>

    </div>
    <!-- End bs-wizard -->
  </div>
  <!-- End intro-title -->
</section>
<!-- End Section hero_2 -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="#">Home</a>
        </li>
        <li><a href="#">Keranjang Saya</a>
        </li>
        <!-- <li>Page active</li> -->
      </ul>
    </div>
  </div>
  <!-- End position -->

  <div class="container margin_60">
    <div class="row">
      <div class="col-lg-8">
        <table class="table table-striped cart-list add_bottom_30">
          <thead>
            <tr>
              <th>
                Produk
              </th>
              <th>
                Jumlah
              </th>
              <th>
                Harga
              </th>
              <th>
                Total
              </th>
              <th>
                Actions
              </th>
            </tr>
          </thead>
          <tbody>
            <?php $item = 0; $berat = 0;?>
            @foreach($dataKeranjangs as $key => $dataKeranjang)
            <?php $item+=$dataKeranjang->jumlah;
            $berat+=$dataKeranjang->produkId->berat_gram*$dataKeranjang->jumlah; ?>
            <tr>
              <td>
                <div class="thumb_cart">
                  <img src="{{asset('img/products/'.$dataKeranjang->produkId->gambarProduk->gambar)}}" alt="Image">
                </div>
                <span class="item_cart">{{$dataKeranjang->produkId->nama}}</span>
              </td>
              <td>
                <div class="numbers-row">
                    <input type="text" value="{{$dataKeranjang->jumlah}}"  class="qty2 form-control" name="quantity_1" class="quantity_1">
                    <input type="text" style="display: none;" name="ids" value="{{$dataKeranjang['id']}}" class="ids">
                    <input type="text" style="display: none;" name="jumlah" value="{{$dataKeranjang['jumlah']}}" class="jumlah">
                    @csrf
                </div>
              </td>
              <td>
                Rp {{number_format($dataKeranjang->produkId->harga)}}
              </td>
              <td>
                <strong>Rp {{number_format($dataKeranjang->produkId->harga*$dataKeranjang->jumlah)}}</strong>
              </td>
              <td class="options">
                <a href="{{route('hapus_keranjang',$dataKeranjang->id)}}"><i class=" icon-trash"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{--<table class="table table-striped options_cart">
          <thead>
            <tr>
              <th colspan="3">
                Add options / Services
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="width:10%">
                <i class="icon_set_1_icon-16"></i>
              </td>
              <td style="width:60%">
                Dedicated Tour guide <strong>+$34</strong>
              </td>
              <td style="width:35%">
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_1" id="option_1" checked value="">
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <i class="icon_set_1_icon-26"></i>
              </td>
              <td>
                Pick up service <strong>+$34*</strong>
              </td>
              <td>
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_2" id="option_2" value="">
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <i class="icon_set_1_icon-71"></i>
              </td>
              <td>
                Insurance <strong>+$24*</strong>
              </td>
              <td>
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_3" id="option_3" value="" checked>
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <i class="icon_set_1_icon-15"></i>
              </td>
              <td>
                Welcome bottle <strong>+$24</strong>
              </td>
              <td>
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_4" id="option_4" value="">
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <i class="icon_set_1_icon-59"></i>
              </td>
              <td>
                Coffe break <strong>+$12*</strong>
              </td>
              <td>
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_5" id="option_5" value="">
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <i class="icon_set_1_icon-58"></i>
              </td>
              <td>
                Dinner <strong>+$26*</strong>
              </td>
              <td>
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_6" id="option_6" value="">
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <i class="icon_set_1_icon-40"></i>
              </td>
              <td>
                Bike rent <strong>+$26*</strong>
              </td>
              <td>
                <label class="switch-light switch-ios float-right">
                  <input type="checkbox" name="option_7" id="option_7" value="">
                  <span>
                            <span>No</span>
                  <span>Yes</span>
                  </span>
                  <a></a>
                </label>
              </td>
            </tr>
          </tbody>
        </table>
        <div class="add_bottom_15"><small>* Prices for person.</small>
        </div>--}}
      </div>
      <!-- End col -->

      <aside class="col-lg-4">
        <div class="box_style_1">
          <h3 class="inner">- Ringkasan -</h3>
          <table class="table table_summary">
            <tbody>
              <tr>
                <td>
                  Jumlah Item
                </td>
                <td class="text-right">
                  {{$item}}
                </td>
              </tr>
              <tr>
                <td>
                  Total Berat
                </td>
                <td class="text-right">
                  {{$berat}} Gram / {{$berat/1000}} Kg
                </td>
              </tr>
              <tr>
                <td>
                  Total Ongkir
                </td>
                <td class="text-right">
                  $34
                </td>
              </tr>
              <tr>
                <td>
                  Insurance
                </td>
                <td class="text-right">
                  $34
                </td>
              </tr>
              <tr class="total">
                <td>
                  Total cost
                </td>
                <td class="text-right">
                  $154
                </td>
              </tr>
            </tbody>
          </table>
          <a class="btn_full" href="payment.html">Check out</a>
          <a class="btn_full_outline" href="#"><i class="icon-right"></i> Continue shopping</a>
        </div>
        <div class="box_style_4">
          <i class="icon_set_1_icon-57"></i>
          <h4>Need <span>Help?</span></h4>
          <a href="tel://004542344599" class="phone">+45 423 445 99</a>
          <small>Monday to Friday 9.00am - 7.30pm</small>
        </div>
      </aside>
      <!-- End aside -->

    </div>
    <!--End row -->
  </div>
  <!--End container -->
</main>
@endsection
@section('javascript')
@foreach($dataKeranjangs as $keys => $data)

<script type="text/javascript">

$(".numbers-row").append('<div class="inc button_inc tambah">+</div><div class="dec button_inc tambah">-</div>');
$(".tambah").on("click", function () {

  var $button = $(this);
  var oldValue = $button.parent().find("input").val();

  if ($button.text() == "+") {
    var newVal = parseFloat(oldValue) + 1;
    console.log(newVal);
  } else {
    // Don't allow decrementing below zero
    if (oldValue > 1) {
      var newVal = parseFloat(oldValue) - 1;
      console.log(newVal);
    } else {
      newVal = 0;
      console.log(newVal);
    }
  }

  $button.parent().find("input[name='quantity_1']").val(newVal);
  $button.parent().find("input[name='jumlah']").val(newVal);

  var ids = $button.parent().find("input[class='ids']").val();
  var jumlah = $button.parent().find("input[class='jumlah']").val();
  var token = $button.parent().find("input[name='_token']").val();
  console.log("IDS : "+ids);
  console.log("JUMLAH : "+jumlah);
  $.ajax({
    url: "<?php echo route('user_edit_keranjang') ?>",
    method:'POST',
    data:{_token:token, ids:ids,jumlah:jumlah},
    success:function(data){
      console.log(data);
      if (data.code=200) {
        location.reload();
        console.log('code:'+data.code);
      }else if (data.code=400){
        console.log('code:'+data.code);
      }
    }
  });
});
</script>
@endforeach
@endsection
