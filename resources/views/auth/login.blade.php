@extends('layouts.umum.app')

@section('content')
<main>
  <section id="hero" class="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                <div id="login">
                      <div class="text-center"><img src="img/dana_mbojo/logo_sticky.png" alt="Image" data-retina="true" ></div>
                          <hr>
                          @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                          @endif
                          <form method="POST" action="{{ route('login') }}">
                              @csrf
                          <a href="{{ url('/auth/facebook') }}" class="social_bt facebook">Login Dengan Facebook</a>
            {{--<a href="{{ url('/auth/google') }}" class="social_bt google">Login with Google</a>--}}
            <div class="divider"><span>Atau</span></div>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
                              <div class="form-group">
                                  <label>Email</label>
                                  <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                  <!-- @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror -->
                              </div>
                              <div class="form-group">
                                  <label>Kata Sandi</label>
                                  <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" value="{{old('password')}}" name="password" required autocomplete="current-password">
                                  <!-- @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror -->
                              </div>
                              <p class="small">
                                  <a href="#">Forgot Password?</a>
                              </p>
                              <button type="submit" class="btn_full">Login</button>
                              <!-- <a href="#" class="btn_full">Sign in</a> -->
                              <a href="{{route('register')}}" class="btn_full_outline">Register</a>
                          </form>
                      </div>
              </div>
          </div>
      </div>
  </section>
</main><!-- End main -->
@endsection
