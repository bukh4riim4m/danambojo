@extends('layouts.umum.app')

@section('content')
<main>
  <section id="hero" class="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                <div id="login">
                      <div class="text-center"><img src="{{asset('img/dana_mbojo/logo_sticky.png')}}" alt="Image" data-retina="true" ></div>

                          <hr>
                          <form method="POST" action="{{ route('password.update') }}">
                              @csrf
                              <input type="hidden" name="token" value="{{ $token }}">
                              <div class="form-group">
                                <label>Email</label>
                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" placeholder="Email" autocomplete="email" autofocus>
                                  @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label>Password</label>
                                  <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ old('password') }}" placeholder="Password" autocomplete="new-password">
                                  @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label>Ulangi password</label>
                                  <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password-confirm" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Ulangi password" autocomplete="new-password">
                                  @error('password_confirmation')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                              </div>
                              <div id="pass-info" class="clearfix"></div>
                              <button type="submit" class="btn_full">S I M P A N</button>
                          </form>
                      </div>
              </div>
          </div>
      </div>
  </section>
</main><!-- End main -->
@endsection
