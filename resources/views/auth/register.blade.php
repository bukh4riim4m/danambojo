@extends('layouts.umum.app')

@section('content')
<main>
  <section id="hero" class="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                <div id="login">
                      <div class="text-center"><img src="img/dana_mbojo/logo_sticky.png" alt="Image" data-retina="true" ></div>

                          <hr>
                          @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                          @endif
                         <form method="POST" action="{{ route('register') }}">
                             @csrf
                              <div class="form-group">
                                <label>Nama Lengkap</label>
                                  <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Nama Lengkap" autocomplete="name" autofocus>
                                  <!-- @error('name')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror -->
                              </div>
                              <div class="form-group">
                                <label>Email</label>
                                  <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" autocomplete="email">
                                  <!-- @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror -->
                              </div>
                              <div class="form-group">
                                <label for="password">Kata Sandi</label>
                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" placeholder="Kata Sandi">
                              </div>
                              <div class="form-group">
                                <label>Ulangi Kata Sandi</label>
                                  <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Ulangi Kata Sandi" autocomplete="new-password">
                                  <!-- @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror -->
                              </div>
                              <div id="pass-info" class="clearfix"></div>
                              <button type="submit" class="btn_full">DAFTAR SEKARANG</button>
                          </form>
                      </div>
              </div>
          </div>
      </div>
  </section>
</main><!-- End main -->
@endsection
