@extends('layouts.umum.app')
@section('icon_cari')
<li>
    <a href="javascript:void(0);" class="search-overlay-menu-btn"><i class="icon_search"></i></a>
</li>
@endsection
@section('form_search')
<div class="search-overlay-menu">
  <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
  <form role="search" id="searchform" method="get">
    <input value="" name="q" type="search" placeholder="Nama Restoran... ?" />
    <button type="submit"><i class="icon_set_1_icon-78"></i>
    </button>
  </form>
</div>
@endsection
@section('content')
<?php $webs = App\Web::find(1); ?>
<section class="parallax-window" data-parallax="scroll" data-image-src="img/bg/restaurant_top.jpg" data-natural-width="1400" data-natural-height="470">
  <div class="parallax-content-1">
    <div class="animated fadeInDown">
      <h1>Mbojo restoran</h1>
      {{--<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>--}}
    </div>
  </div>
</section>
<!-- End section -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="#">Home</a>
        </li>
        <li><a href="#">Category</a>
        </li>
        <li>Page active</li>
      </ul>
    </div>
  </div>
  <!-- Position -->

  <div class="collapse" id="collapseMap">
    <div id="map" class="map"></div>
  </div>
  <!-- End Map -->

  <div class="container margin_60">

    <div class="row">
      <aside class="col-lg-3">
        <p>
          <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on map</a>
        </p>

        <div class="box_style_cat">
          <ul id="cat_nav">
            <li><a href="#" id="active"><i class="icon_set_3_restaurant-10"></i>All restaurants <span>(141)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-1"></i>Pizza / Italian <span>(20)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-2"></i>Fast Food <span>(16)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-3"></i>Japanese <span>(12)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-4"></i>Chinese <span>(11)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-5"></i>International <span>(20)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-8"></i>Coffe bar <span>(08)</span></a>
            </li>
            <li><a href="#"><i class="icon_set_3_restaurant-7"></i>Fish <span>(08)</span></a>
            </li>
          </ul>
        </div>

        <div id="filters_col">
          <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Filters</a>
          <div class="collapse show" id="collapseFilters">
            <div class="filter_type">
              <h6>Price</h6>
              <input type="text" id="range" name="range" value="">
            </div>
            <div class="filter_type">
              <h6>Rating</h6>
              <ul>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"><span class="rating">
                    <i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
                    </span>
                  </label>
                </li>
              </ul>
            </div>
            <div class="filter_type">
              <h6>District/Area</h6>
              <ul>
                <li>
                  <label>
                    <input type="checkbox">Paris Centre
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox">La Defance
                  </label>
                </li>
                <li>
                  <label>
                    <input type="checkbox">Latin Quarter
                  </label>
                </li>
              </ul>
            </div>
          </div>
          <!--End collapse -->
        </div>
        <!--End filters col-->
        <div class="box_style_2">
          <i class="icon_set_1_icon-57"></i>
          <h4>Need <span>Help?</span></h4>
          <a href="tel://004542344599" class="phone">{{$webs->phone}}</a>
          <small>Monday to Friday 9.00am - 7.30pm</small>
        </div>
      </aside>
      <!--End aside -->
      <div class="col-lg-9">

        <div id="tools">

          <div class="row">
            <div class="col-md-3 col-sm-4 col-6">
              <div class="styled-select-filters">
                <select name="sort_price" id="sort_price">
                  <option value="" selected>Sort by price</option>
                  <option value="lower">Lowest price</option>
                  <option value="higher">Highest price</option>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-4 col-6">
              <div class="styled-select-filters">
                <select name="sort_rating" id="sort_rating">
                  <option value="" selected>Sort by ranking</option>
                  <option value="lower">Lowest ranking</option>
                  <option value="higher">Highest ranking</option>
                </select>
              </div>
            </div>
            <div class="col-md-6 col-sm-4 d-none d-sm-block text-right">
              <a href="all_restaurants_grid.html" class="bt_filters"><i class="icon-th"></i></a> <a href="#" class="bt_filters"><i class="icon-list"></i></a>
            </div>

          </div>
        </div>
        <!--/tools -->

        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="ribbon_3 popular"><span>Popular</span>
              </div>
              <div class="wishlist">
                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
              </div>
              <div class="img_list">
                <a href="single_restaurant.html"><img src="img/restaurant_box_1.jpg" alt="Image">
                  <div class="short_info"><i class="icon_set_3_restaurant-2"></i> Fast food</div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="tour_list_desc">
                <div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <h3><strong>King food</strong> restaurant</h3>
                <p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex, qui adipisci maiestatis inciderint no, eos in elit dicat.....</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
                      <div class="tooltip-content">
                        <h4>Disabled</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-47"></i></span>
                      <div class="tooltip-content">
                        <h4>No smoking area</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Parking</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Transport</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="price_list">
                <div><sup>$</sup>39*<span class="normal_price_list">$99</span><small>*Per person</small>
                  <p><a href="single_restaurant.html" class="btn_1">Details</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->

        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.2s">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="ribbon_3 popular"><span>Popular</span>
              </div>
              <div class="wishlist">
                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
              </div>
              <div class="img_list">
                <a href="single_restaurant.html"><img src="img/restaurant_box_2.jpg" alt="Image">
                  <div class="short_info"><i class="icon_set_3_restaurant-2"></i> Fast food</div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="tour_list_desc">
                <div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <h3><strong>Catrine</strong> restaurant</h3>
                <p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex, qui adipisci maiestatis inciderint no, eos in elit dicat.....</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
                      <div class="tooltip-content">
                        <h4>Disabled</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-47"></i></span>
                      <div class="tooltip-content">
                        <h4>No smoking area</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Parking</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Transport</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="price_list">
                <div><sup>$</sup>42*<span class="normal_price_list">$99</span><small>*Per person</small>
                  <p><a href="single_restaurant.html" class="btn_1">Details</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->

        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.3s">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="ribbon_3 popular"><span>Popular</span>
              </div>
              <div class="wishlist">
                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
              </div>
              <div class="img_list">
                <a href="single_restaurant.html"><img src="img/restaurant_box_3.jpg" alt="Image">
                  <div class="short_info"><i class="icon_set_3_restaurant-1"></i> Pizza / Italian</div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="tour_list_desc">
                <div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <h3><strong>Bella Napoli</strong> restaurant</h3>
                <p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex, qui adipisci maiestatis inciderint no, eos in elit dicat.....</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
                      <div class="tooltip-content">
                        <h4>Disabled</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-47"></i></span>
                      <div class="tooltip-content">
                        <h4>No smoking area</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Parking</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Transport</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="price_list">
                <div><sup>$</sup>39*<span class="normal_price_list">$99</span><small>*Per person</small>
                  <p><a href="single_restaurant.html" class="btn_1">Details</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->

        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.4s">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="ribbon_3"><span>Top rated</span>
              </div>
              <div class="wishlist">
                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
              </div>
              <div class="img_list">
                <a href="single_restaurant.html"><img src="img/restaurant_box_4.jpg" alt="Image">
                  <div class="short_info"><i class="icon_set_3_restaurant-4"></i> Chinese</div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="tour_list_desc">
                <div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <h3><strong>Dragon tower</strong> restaurant</h3>
                <p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex, qui adipisci maiestatis inciderint no, eos in elit dicat.....</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
                      <div class="tooltip-content">
                        <h4>Disabled</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-47"></i></span>
                      <div class="tooltip-content">
                        <h4>No smoking area</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Parking</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Transport</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="price_list">
                <div><sup>$</sup>69*<span class="normal_price_list">$59</span><small>*Per person</small>
                  <p><a href="single_restaurant.html" class="btn_1">Details</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->

        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.5s">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="ribbon_3"><span>Top rated</span>
              </div>
              <div class="wishlist">
                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
              </div>
              <div class="img_list">
                <a href="single_restaurant.html"><img src="img/restaurant_box_5.jpg" alt="Image">
                  <div class="short_info"><i class="icon_set_3_restaurant-7"></i> Fish</div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="tour_list_desc">
                <div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <h3><strong>Sea food</strong> restaurant</h3>
                <p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex, qui adipisci maiestatis inciderint no, eos in elit dicat.....</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
                      <div class="tooltip-content">
                        <h4>Disabled</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-47"></i></span>
                      <div class="tooltip-content">
                        <h4>No smoking area</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Parking</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Transport</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="price_list">
                <div><sup>$</sup>49*<span class="normal_price_list">$59</span><small>*Per person</small>
                  <p><a href="single_restaurant.html" class="btn_1">Details</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->

        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.7s">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="ribbon_3"><span>Top rated</span>
              </div>
              <div class="wishlist">
                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
              </div>
              <div class="img_list">
                <a href="single_restaurant.html"><img src="img/restaurant_box_6.jpg" alt="Image">
                  <div class="short_info"><i class="icon_set_3_restaurant-5"></i> International</div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="tour_list_desc">
                <div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
                </div>
                <h3><strong>Alfredo</strong> restaurant</h3>
                <p>Lorem ipsum dolor sit amet, quem convenire interesset ut vix, ad dicat sanctus detracto vis. Eos modus dolorum ex, qui adipisci maiestatis inciderint no, eos in elit dicat.....</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-13"></i></span>
                      <div class="tooltip-content">
                        <h4>Disabled</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-47"></i></span>
                      <div class="tooltip-content">
                        <h4>No smoking area</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Parking</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Transport</h4> Usu in novum nostrud disputando, ei quo aperiri omittam vidit fastidii.
                        <br>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="price_list">
                <div><sup>$</sup>49*<span class="normal_price_list">$59</span><small>*Per person</small>
                  <p><a href="single_restaurant.html" class="btn_1">Details</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->

        <hr>

        <nav aria-label="Page navigation">
          <ul class="pagination justify-content-center">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item active"><span class="page-link">1<span class="sr-only">(current)</span></span>
            </li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- end pagination-->

      </div>
      <!-- End col lg-9 -->
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</main>
<!-- End main -->
@endsection
@section('javascript')
<!-- Cat nav mobile -->
<script src="js/cat_nav_mobile.js"></script>
<script>
  $('#cat_nav').mobileMenu();
</script>

<!-- Map -->
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="js/map_restaurants.js"></script>
<script src="js/infobox.js"></script>

<!-- Check box and radio style iCheck -->
<script>
  $('input').iCheck({
     checkboxClass: 'icheckbox_square-grey',
     radioClass: 'iradio_square-grey'
   });
</script>
@endsection
