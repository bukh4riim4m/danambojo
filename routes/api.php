<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('api_login', 'API\UmumController@login');
Route::post('api_register', 'API\UmumController@register');
Route::get('api_slider', 'API\UmumController@slider');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('api_details', 'API\UserController@details');
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
