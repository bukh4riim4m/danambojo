<?php
Route::get('tio_pu_log', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('commingson',['as'=>'commingson','uses'=>'UmumController@commingson']);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/keluar', ['as'=>'keluar','uses'=>'UserController@keluar']);
// Route::get('/', function () {
//     return view('welcome');te
// });
Route::get('/', ['as'=>'dasboard','uses'=>'UmumController@dasboard']);
Route::get('/tours', ['as'=>'tours','uses'=>'UmumController@tours']);
Route::get('/tiket-wisata', ['as'=>'tiket_wisata','uses'=>'UmumController@tiketwisata']);
Route::get('/hotels', ['as'=>'hotels','uses'=>'UmumController@hotels']);
Route::get('/restoran', ['as'=>'restoran','uses'=>'UmumController@restoran']);
Route::get('/toko', ['as'=>'semua_toko','uses'=>'UmumController@semuatoko']);
Route::get('/toko/{id}', ['as'=>'toko','uses'=>'UmumController@toko']);
Route::get('/produk', ['as'=>'semua_produk','uses'=>'UmumController@semuaproduk']);
Route::get('/produk/{id}', ['as'=>'single_produk','uses'=>'UmumController@singleproduk']);
Route::get('/single-tour', ['as'=>'single_tour','uses'=>'UmumController@singletour']);
Route::get('/single-hotel', ['as'=>'single_hotel','uses'=>'UmumController@singlehotel']);
Route::get('/blog', ['as'=>'blog','uses'=>'UmumController@blog']);
Route::get('/blog/{detail}', ['as'=>'detail-blog','uses'=>'UmumController@detailblog']);
Route::get('/hubungi-kami', ['as'=>'hubungi_kami','uses'=>'UmumController@contact']);
Route::get('/tentang-kami', ['as'=>'tentang_kami','uses'=>'UmumController@about']);
Route::get('/pertanyaan', ['as'=>'pertanyaan','uses'=>'UmumController@faq']);
Route::get('/gallery', ['as'=>'gallery','uses'=>'UmumController@gallery']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//LOGIN SOSMED
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

//ROUTE USER
Route::get('/toko_saya', ['as'=>'toko_saya','uses'=>'UserController@tokosaya']);
Route::get('/toko_saya/{id}', ['as'=>'produk_saya','uses'=>'UserController@produksaya']);
Route::get('/detail_produk_saya/{id}', ['as'=>'detail_produk_saya','uses'=>'UserController@detailproduksaya']);
Route::post('/detail_produk_saya/{id}', ['as'=>'detail_produk_saya','uses'=>'UserController@detailproduksaya']);
//Masuk Keranjang
Route::get('/keranjang', ['as'=>'keranjang','uses'=>'UserController@keranjang']);
Route::post('/keranjang/{id}', ['as'=>'updatekeranjang','uses'=>'UserController@updatekeranjang']);
Route::get('/masuk_keranjang/{id}', ['as'=>'masuk_keranjang','uses'=>'UserController@masukkeranjang']);
Route::post('/edit_keranjang', ['as'=>'user_edit_keranjang','uses'=>'UserController@usereditkeranjang']);
Route::get('/hapus_keranjang/{ids}', ['as'=>'hapus_keranjang','uses'=>'UserController@hapuskeranjang']);

//SHARE FB
Route::get('/share', ['as'=>'share','uses'=>'UserController@share']);
