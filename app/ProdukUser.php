<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukUser extends Model
{
  protected $fillable = [
      'id','user_id', 'toko_id', 'kode','nama','kondisi','harga','kategori_one_id','kategori_two_id','terkirim','min_beli','berat_gram','informasi','stok','dilihat','aktif'
  ];
  public function gambarProduk(){
    return $this->hasOne('App\GambarProduk','produk_user_id');
  }
  public function kategoriOneId(){
    return $this->belongsTo('App\KategoriOne','kategori_one_id');
  }
  public function kategoriTwoId(){
    return $this->belongsTo('App\KategoriTwo','kategori_two_id');
  }
  public function tokoId(){
    return $this->belongsTo('App\Toko','toko_id');
  }
}
