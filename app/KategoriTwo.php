<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriTwo extends Model
{
  protected $fillable = [
      'id','kategori_one_id', 'kategori','aktif'
  ];

}
