<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriOne extends Model
{
  protected $fillable = [
      'id', 'kategori','aktif'
  ];
}
