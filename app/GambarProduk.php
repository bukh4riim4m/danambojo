<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GambarProduk extends Model
{
  protected $fillable = [
      'id', 'user_id','toko_id','produk_user_id','gambar','aktif'
  ];
}
