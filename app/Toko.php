<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
  protected $fillable = [
      'user_id', 'tgl_daftar', 'pemilik_toko','nama_toko','telpon','gambar','kabupaten','kecamatan','desa','alamat_lengkap','reting','aktif'
  ];
}
