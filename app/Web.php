<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
  protected $fillable = [
      'id', 'judul','logo','phone','email'
  ];
  public function alamatWeb(){
    return $this->hasOne('App\alamatWebs','id');
  }
}
