<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
  protected $fillable = [
      'id', 'user_id','toko_id','produk_user_id','diskon','jumlah','admin_id','aktif'
  ];
  public function produkId(){
    return $this->belongsTo('App\ProdukUser','produk_user_id');
  }
}
