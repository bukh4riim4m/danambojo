<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarProduk extends Model
{
  protected $fillable = [
      'produk_user_id', 'reting','user_komen','komentar','aktif'
  ];
  public function userId(){
    return $this->belongsTo('App\User','user_komen');
  }
}
