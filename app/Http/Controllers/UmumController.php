<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Toko;
use App\ProdukUser;
use App\KomentarProduk;
use App\GambarProduk;
use Auth;

class UmumController extends Controller
{
  public function dasboard(){
    if (Auth::user()) {
      if (Auth::user()->type ==  'admin') {
        return view('admin.index');
      }
      return view('welcome');
    }else {
      return view('welcome');
    }
  }
  
  public function contact()
  {
      return view('contact_us');
  }
  public function about()
  {
      return view('about_us');
  }
  public function tours()
  {
      return view('tours');
  }
  public function tiketwisata()
  {
      return view('tiket_wisata');
  }
  public function hotels()
  {
      return view('hotels');
  }
  public function restoran()
  {
      return view('restoran');
  }
  public function semuatoko(Request $request)
  {
      $tokos = Toko::get();
      return view('all_toko',compact('tokos'));
  }
  public function semuaproduk()
  {
      return view('semua_produk');
  }
  public function singleproduk($id)
  {
      $details = ProdukUser::where('aktif','yes')->where('id',$id)->first();
      $comentars = KomentarProduk::where('produk_user_id',$id)->where('aktif','yes')->get();
      $gambars = GambarProduk::where('produk_user_id',$details->id)->where('aktif','yes')->get();
      return view('single_produk',compact('details','comentars','gambars'));
  }
  public function toko($id)
  {
      $tokos = Toko::find($id);
      $produks = ProdukUser::where('toko_id',$id)->where('aktif','yes')->get();
      return view('single_toko',compact('tokos','produks'));
  }
  public function singletour()
  {
      return view('single_tour');
  }
  public function singlehotel()
  {
      return view('single_hotel');
  }
  public function blog()
  {
      return view('blog');
  }
  public function detailblog($detail){
    return view('single_blog');
  }
  public function faq()
  {
      return view('faq');
  }
  public function commingson(){
    return view('commingson');
  }
  public function gallery(){
    return view('gallery_tiga_kolom');
  }
}
