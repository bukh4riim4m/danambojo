<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      //Log::info($data);
      $message = [
        'name.required'=>'Nama Lengkap tidak boleh kosong',
        'email.required'=>'Email tidak boleh kosong',
        'email.unique'=>'Email ini sudah di gunakan',
        'password.required'=>'Kata sandi tidak boleh kosong',
        'password.min'=>'Kata sandi minimal 6 digit',
        'password.confirmed'=>'Kata sandi tidak cocok',
        'password_confirmation.required'=>'Ulangi Kata sandi tidak boleh kosong',
        'password_confirmation.confirmed'=>'Ulangi Kata sandi tidak cocok',
        'password_confirmation.min'=>'Ulangi Kata sandi minimal 6 digit'
      ];
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6','confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6']
        ],$message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'type' => 'pembeli',
            'aktif' => 1,
            'password' => Hash::make($data['password_confirmation']),
        ]);
    }
}
