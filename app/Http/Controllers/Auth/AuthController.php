<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Log;
use Mail;
use DB;

class AuthController extends Controller
{
  public function redirectToProvider($provider)
  {
      return Socialite::driver($provider)->redirect();
  }

  /**
   * Obtain the user information from provider.  Check if the user already exists in our
   * database by looking up their provider_id in the database.
   * If the user exists, log them in. Otherwise, create a new user then log them in. After that
   * redirect them to the authenticated users homepage.
   *
   * @return Response
   */
  public function handleProviderCallback($provider)
  {
      $user = Socialite::driver($provider)->user();
      $authUser = $this->findOrCreateUser($user, $provider);
      Auth::login($authUser, true);
      return redirect('/');
  }

  public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->scopes([
            "publish_actions, manage_pages", "publish_pages"])->redirect();
    }
  /**
   * If a user has registered before using social auth, return the user
   * else, create a new user object.
   * @param  $user Socialite user object
   * @param $provider Social auth provider
   * @return  User
   */
  public function findOrCreateUser($user, $provider)
  {
    Log::info('Respon FB:'.$provider);
      $authUser = User::where('provider_id', $user->id)->first();
      if ($authUser) {
        Log::info('Respon FBL:'.$authUser);
          return $authUser;
      }
      else{
        if($user_id = User::where('email', $user->email)->first()){
          $data = User::find($user_id->id)->update([
              'name'     => $user->name,
              'email'    => !empty($user->email)? $user->email : '' ,
              'foto'     => $user->avatar,
              'provider' => $provider,
              'provider_id' => $user->id
          ]);
        }else {
          $pass = rand(100000,999999);
          DB::beginTransaction();
          try {
            Mail::send('email', ['nama' => 'Dana Mbojo', 'pesan' => 'Password Dana Mbojo Anda : '.$pass], function ($message) use ($user)
            {
                $message->subject('Password untuk login Dana Mbojo');
                $message->from('d4n4mbojo@gmail.com', 'Dana Mbojo');
                $message->to($user->email);
            });

            $data = User::create([
                'name'     => $user->name,
                'email'    => !empty($user->email)? $user->email : '' ,
                'foto' =>$user->avatar,
                'password'=>Hash::make($pass),
                'aktif'=>1,
                'type'=>'pembeli',
                'provider' => $provider,
                'provider_id' => $user->id
            ]);
          } catch (\Exception $e) {
            Log::info('Gagal Edit Profil:'.$e->getMessage());
            DB::rollback();
            return $data;
          }
          DB::commit();
          return $data;
        }
      }
  }
}
