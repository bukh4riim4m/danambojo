<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Toko;
use App\ProdukUser;
use App\GambarProduk;
use App\KomentarProduk;
use App\Keranjang;
use Session;
use Log;
use DB;
use Share;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  public function __construct()
  {
      $this->middleware('pembeli');
  }
  public function keluar(Request $request){
    Auth::logout();
    Session::flush();
    return redirect()->route('dasboard');
  }
  public function tokosaya(Request $request)
  {
    $tokos = Toko::where('user_id',$request->user()->id)->get();
    return view('user.toko_saya',compact('tokos'));
  }
  public function produksaya(Request $request,$id)
  {
    $tokos = Toko::where('user_id',$request->user()->id)->where('id',$id)->where('aktif','yes')->first();
    $produks = ProdukUser::where('user_id',$request->user()->id)->where('toko_id',$id)->where('aktif','yes')->get();
    return view('user.produk_saya',compact('produks','tokos'));
  }
  public function detailproduksaya(Request $request,$id)
  {
    if ($request->action == 'add_komen') {
      $message = [
        'action.required'=>'wajib diisi',
        '_token.required'=>'wajib diisi',
        'komentar.required'=>'Komentar wajib diisi',
        'reting.required'=>'Reting Wajib dipilih',
        'reting.max'=>'Reting Wajib dipilih'
      ];
      $validasi = $request->validate([
        'action' => 'required|string|max:255',
        '_token' => 'required',
        'reting' => 'required|max:2',
        'komentar' => 'required|min:3',
      ],$message);
      $add = KomentarProduk::create([
        'produk_user_id'=>$id,
        'reting'=>$request->reting,
        'komentar'=>$request->komentar,
        'user_komen'=>$request->user()->id,
        'aktif'=>'yes'
      ]);
      if ($add) {
        return redirect()->back();
      }
      return redirect()->back();
    }

    $details = ProdukUser::where('user_id',$request->user()->id)->where('aktif','yes')->where('id',$id)->first();
    $comentars = KomentarProduk::where('produk_user_id',$id)->where('aktif','yes')->get();
    $gambars = GambarProduk::where('user_id',$request->user()->id)->where('produk_user_id',$details->id)->where('aktif','yes')->get();
    return view('user.detail_produk_saya',compact('details','gambars','comentars'));
  }
  public function keranjang(Request $request){
    $dataKeranjangs = Keranjang::where('user_id',$request->user()->id)->get();
    return view('user.keranjang',compact('dataKeranjangs'));
  }
  public function updatekeranjang(Request $request,$id){
    if ($request->jumlah) {
      $dataKeranjangs = Keranjang::where('user_id',$request->user()->id)->where('id',$id)->first();
      $dataKeranjangs->jumlah = $request->jumlah;
      //return $request->jumlah.'-'.$dataKeranjangs->produkId->stok;
      if($request->jumlah < $dataKeranjangs->produkId->stok){
        $dataKeranjangs->update();
      }
    }
    return redirect()->back();
  }
  public function masukkeranjang(Request $request,$id){
    $produks = ProdukUser::find($id);
    DB::beginTransaction();
    try {
      Keranjang::create([
        'user_id'=>$request->user()->id,
        'toko_id'=>$produks->toko_id,
        'produk_user_id'=>$produks->id,
        'diskon'=>0,
        'jumlah'=>1,
        'aktif'=>'yes',
        'admin_id'=>2
      ]);
    } catch (\Exception $e) {
      Log::info('Gagal Edit Profil:'.$e->getMessage());
      DB::rollback();
      return 'gagal';
    }
    DB::commit();
    return redirect()->back();
  }
  public function share(){
    Share::page('http://jorenvanhocht.be')->whatsapp();
  }
  public function usereditkeranjang(Request $request){
    if ($request->ajax()) {
      $datas = Keranjang::find($request['ids']);
      $datas->jumlah = $request->jumlah;
      if ($datas->update()) {
        $data = [
          'code'=>200,
          'message'=>'Berhasil'
        ];
      }else {
        $data = [
          'code'=>400,
          'message'=>'Gagal'
        ];
      }

      return response()->json($data);
    }
  }
  public function hapuskeranjang($ids){
    if ($datas = Keranjang::where('id',$ids)->where('user_id',Auth::user()->id)->first()) {
      $datas->delete();
      return redirect()->back();
    }
    return redirect()->back();
  }
}
