<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentarProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_produks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('produk_user_id');
            $table->foreign('produk_user_id')->references('id')->on('produk_users');
            $table->integer('reting');
            $table->text('komentar');
            $table->unsignedBigInteger('user_komen');
            $table->foreign('user_komen')->references('id')->on('users');
            $table->enum('aktif', ['yes', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_produks');
    }
}
