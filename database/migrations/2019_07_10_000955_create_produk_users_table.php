<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdukUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('toko_id');
            $table->foreign('toko_id')->references('id')->on('tokos');
            $table->unsignedBigInteger('kategori_one_id');
            $table->foreign('kategori_one_id')->references('id')->on('kategori_ones');
            $table->unsignedBigInteger('kategori_two_id');
            $table->foreign('kategori_two_id')->references('id')->on('kategori_twos');
            $table->string('kode',10);
            $table->char('nama',100);
            $table->enum('kondisi', ['baru', 'bekas']);
            $table->integer('harga');
            $table->integer('terkirim');
            $table->integer('min_beli');
            $table->integer('berat_gram');
            $table->text('informasi');
            $table->integer('stok');
            $table->integer('dilihat');
            $table->enum('aktif', ['yes', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_users');
    }
}
