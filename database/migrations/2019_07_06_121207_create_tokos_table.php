<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->date('tgl_daftar');
            $table->string('pemilik_toko',100);
            $table->char('nama_toko',100);
            $table->string('telpon',20);
            $table->string('gambar',100);
            $table->string('kabupaten',100);
            $table->string('kecamatan',100);
            $table->string('desa',100);
            $table->text('alamat_lengkap');
            $table->enum('reting', ['1', '2','3','4','5']);
            $table->enum('aktif', ['yes', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokos');
    }
}
