<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;

class TokoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = User::where('type','pembeli')->where('aktif',1)->get();
      foreach ($users as $key => $user) {
        DB::table('tokos')->insert(
          [
            'id'      => 1,
            'user_id'       => $user->id,
            'tgl_daftar'       => Carbon::now(),
            'pemilik_toko'       => $user->name,
            'nama_toko'       => 'Sahabat Rakyat',
            'telpon'       => '082312543008',
            'gambar'       => 'toko_eka.png',
            'kabupaten'       => 'bima',
            'kecamatan'       => 'sape',
            'desa'       => 'rasa bou',
            'alamat_lengkap'       => 'Jl. Rasabou ma wou',
            'reting'       => 4,
            'aktif'      => 'yes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      }

    }
}
