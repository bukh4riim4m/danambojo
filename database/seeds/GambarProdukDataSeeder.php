<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class GambarProdukDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('gambar_produks')->insert(
        [
          'id'      => 1,
          'user_id'      => 1,
          'toko_id'      => 1,
          'produk_user_id'       => 1,
          'gambar'       => 'image-1.jpg',
          'aktif'      => 'yes',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
      DB::table('gambar_produks')->insert(
        [
          'id'      => 2,
          'user_id'      => 1,
          'toko_id'      => 1,
          'produk_user_id'       => 1,
          'gambar'       => 'image-2.jpg',
          'aktif'      => 'yes',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
      DB::table('gambar_produks')->insert(
        [
          'id'      => 3,
          'user_id'      => 1,
          'toko_id'      => 1,
          'produk_user_id'       => 2,
          'gambar'       => 'image-3.jpg',
          'aktif'      => 'yes',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
      DB::table('gambar_produks')->insert(
        [
          'id'      => 4,
          'user_id'      => 1,
          'toko_id'      => 1,
          'produk_user_id'       => 2,
          'gambar'       => 'image-4.jpg',
          'aktif'      => 'yes',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
    }
}
