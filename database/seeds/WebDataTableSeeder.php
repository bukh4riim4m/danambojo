<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WebDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('webs')->insert(
        [
          'id'      => 1,
          'judul'       => 'Dana Mbojo',
          'logo'      => 'logo.png',
          'phone'      => '+62 823-1254-3008',
          'email'      => 'bukhariimam44@gmail.com',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
    }
}
