<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class KategoriTwoDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $keperluanrumah_id_1 = ['Buku','Alat Dapur','Alat Mandi','Alat Tulis'];
      foreach ($keperluanrumah_id_1 as $key => $value) {
        DB::table('kategori_twos')->insert(
          [
            'id'      => $key+1,
            'kategori'       => $value,
            'kategori_one_id'       => 1,
            'aktif'      => 'yes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      }
      $peralatanelektronik_id_2 = ['Handphone','Leptop','Kamera','Tablet'];
      foreach ($peralatanelektronik_id_2 as $key1 => $peralatanelektronik) {
        DB::table('kategori_twos')->insert(
          [
            'id'      => $key+$key1+2,
            'kategori'       => $peralatanelektronik,
            'kategori_one_id'       => 2,
            'aktif'      => 'yes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      }
      $fashion_wanita_id_3 = ['Fashion Muslim','Pakaian Wanita','Hijab','Bawahan Wanita'];
      foreach ($fashion_wanita_id_3 as $key2 => $fashion_wanita) {
        DB::table('kategori_twos')->insert(
          [
            'id'      => $key+$key1+$key2+3,
            'kategori'       => $fashion_wanita,
            'kategori_one_id'       => 3,
            'aktif'      => 'yes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      }
      $fashion_pria_id_4 = ['Jaket Pria','Atasan Pria','Batik & Baju Muslim','Celana Pria','Sepatu','Celana Dalam'];
      foreach ($fashion_pria_id_4 as $key3 => $fashion_pria) {
        DB::table('kategori_twos')->insert(
          [
            'id'      => $key+$key1+$key2+$key3+4,
            'kategori'       => $fashion_pria,
            'kategori_one_id'       => 4,
            'aktif'      => 'yes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      }

    }
}
