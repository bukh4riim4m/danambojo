<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProdukUserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('produk_users')->insert(
        [
          'id'      => 1,
          'user_id'       => 1,
          'toko_id'       => 1,
          'kode'       => 'B-HBS',
          'nama'       => 'Buku Sejarah',
          'harga'       => 60000,
          'terkirim'       => 0,
          'min_beli'       => 1,
          'berat_gram'       => 500,
          'informasi'       => 'Warna kertas putih, tebal 2 Cm dan anti air.',
          'stok'       => 5,
          'dilihat'       => 2,
          'kategori_one_id'       => 1,
          'kategori_two_id'       => 1,
          'aktif'      => 'yes',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
      DB::table('produk_users')->insert(
        [
          'id'      => 2,
          'user_id'       => 1,
          'toko_id'       => 1,
          'kode'       => 'B-DHG',
          'nama'       => 'Buku Biologi',
          'harga'       => 100000,
          'terkirim'       => 0,
          'min_beli'       => 1,
          'berat_gram'       => 700,
          'informasi'       => 'Warna kertas putih, tebal 2 Cm dan anti bakar.',
          'stok'       => 4,
          'dilihat'       => 2,
          'kategori_one_id'       => 1,
          'kategori_two_id'       => 1,
          'aktif'      => 'yes',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
    }
}
