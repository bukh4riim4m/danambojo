<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
      * Membuat Data Seeder : php artisan make:seeder NamaDataSeeder
      * Menjalankan Seeder : composer dump-autoload
      * Menginput data seeder ke databae : php artisan db:seed --class=NamaDataSeeder

     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(WebDataTableSeeder::class);
        $this->call(KategoriOneDataTableSeeder::class);
        $this->call(KategoriTwoDataTableSeeder::class);
        $this->call(TokoDataSeeder::class);
        $this->call(ProdukUserDataSeeder::class);
        $this->call(GambarProdukDataSeeder::class);
    }
}
