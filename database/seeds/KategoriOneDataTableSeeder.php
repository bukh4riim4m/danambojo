<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class KategoriOneDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $kategoris = ['Keperluan Rumah','Peralatan Elektronik','Fashion Wanita','Fashion Pria'];
      foreach ($kategoris as $key => $kategori) {
        DB::table('kategori_ones')->insert(
          [
            'id'      => $key+1,
            'kategori'       => $kategori,
            'aktif'      => 'yes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      }

    }
}
