<?php
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert(
        [
          'id'      => 1,
          'name'       => 'Imam bukhari',
          'email'      => 'bukhariimam44@gmail.com',
          'foto'      => null,
          'email_verified_at'      => null,
          'password'      => Hash::make(123456),
          'provider'      => null,
          'provider_id'      => null,
          'type'       => 'pembeli',
          'aktif'       => 1,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
      DB::table('users')->insert([
          'id'      => 2,
          'name'       => 'Eka Yuliana',
          'email'      => 'yuliana.3ka@gmail.com',
          'foto'      => null,
          'email_verified_at'      => null,
          'password'      => Hash::make(123456),
          'provider'      => null,
          'provider_id'      => null,
          'type'       => 'admin',
          'aktif'       => 1,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
    }
}
