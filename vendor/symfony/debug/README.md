Debug Component
===============

The Debug component provides tools to ease debugging PHP code.

Resources
---------

  * [Documentation](https://symfony.com/doc/current/components/debug/{{url('/')}})
  * [Contributing](https://symfony.com/doc/current/contributing/{{url('/')}})
  * [Report issues](https://github.com/symfony/symfony/issues) and
    [send Pull Requests](https://github.com/symfony/symfony/pulls)
    in the [main Symfony repository](https://github.com/symfony/symfony)
